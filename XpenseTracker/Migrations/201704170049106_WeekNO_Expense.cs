namespace XpenseTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WeekNO_Expense : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Expenses", "WeekNo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Expenses", "WeekNo");
        }
    }
}
