namespace XpenseTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Expense_Model : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Expenses",
                c => new
                    {
                        ExpenseID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Amount = c.Double(nullable: false),
                        ExpenseDate = c.DateTime(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ExpenseID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Expenses", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Expenses", new[] { "User_Id" });
            DropTable("dbo.Expenses");
        }
    }
}
