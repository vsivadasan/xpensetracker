// <auto-generated />
namespace XpenseTracker.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class WeekNO_Expense : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(WeekNO_Expense));
        
        string IMigrationMetadata.Id
        {
            get { return "201704170049106_WeekNO_Expense"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
