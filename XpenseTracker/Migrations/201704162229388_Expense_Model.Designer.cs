// <auto-generated />
namespace XpenseTracker.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Expense_Model : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Expense_Model));
        
        string IMigrationMetadata.Id
        {
            get { return "201704162229388_Expense_Model"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
