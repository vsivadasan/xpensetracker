﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(XpenseTracker.Startup))]
namespace XpenseTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
