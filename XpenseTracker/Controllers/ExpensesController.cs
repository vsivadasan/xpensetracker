﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using XpenseTracker.Models;

namespace XpenseTracker.Controllers
{
    public class ExpensesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Expenses
        [Authorize]
        public ActionResult Index()
        {
            var id = User.Identity.GetUserId();
            var Bills = db.Expenses.Where(x => x.User.Id == id).ToList();
            List<ExpenseViewModels> l1 = new List<ExpenseViewModels>();
            foreach (var item in Bills)
            {
                l1.Add(new ExpenseViewModels { Description = item.Description, ExpenseAmount = item.Amount, ExpenseDate = item.ExpenseDate, ExpenseId = item.ExpenseID });
            }
            return View(l1);
        }

        // GET: Expenses/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expense expense = db.Expenses.Find(id);
            if (expense == null)
            {
                return HttpNotFound();
            }
            ExpenseViewModels v1 = new ExpenseViewModels { Description = expense.Description, ExpenseAmount = expense.Amount, ExpenseDate = expense.ExpenseDate, ExpenseId = expense.ExpenseID };
            return View(v1);
        }

        // GET: Expenses/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

            //Create an expense
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Description,ExpenseAmount,ExpenseDate")] ExpenseViewModels model)
        {
            Expense expense = new Expense();
            if (ModelState.IsValid)
            {
                // Gets the Calendar instance associated with a CultureInfo.
                CultureInfo myCI = new CultureInfo("en-US");
                Calendar myCal = myCI.Calendar;

                // Gets the DTFI properties required by GetWeekOfYear.
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
                var WeekNo = myCal.GetWeekOfYear(model.ExpenseDate, myCWR, myFirstDOW);
                expense = new Expense { Amount = model.ExpenseAmount, Description = model.Description, ExpenseDate = model.ExpenseDate, WeekNo = WeekNo };
                expense.User = db.Users.Find(User.Identity.GetUserId());
                db.Expenses.Add(expense);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }
        [Authorize]
        // GET: Expenses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expense expense = db.Expenses.Find(id);
            if (expense == null)
            {
                return HttpNotFound();
            }

            ExpenseViewModels v1 = new ExpenseViewModels { Description = expense.Description, ExpenseAmount = expense.Amount, ExpenseDate = expense.ExpenseDate, ExpenseId = expense.ExpenseID };
            return View(v1);
        }

        // POST: Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ExpenseId,Description,ExpenseAmount,ExpenseDate")] ExpenseViewModels model)
        {
            Expense expense = new Expense();
            if (ModelState.IsValid)
            {
                // Gets the Calendar instance associated with a CultureInfo.
                CultureInfo myCI = new CultureInfo("en-US");
                Calendar myCal = myCI.Calendar;

                // Gets the DTFI properties required by GetWeekOfYear.
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
                var WeekNo = myCal.GetWeekOfYear(model.ExpenseDate, myCWR, myFirstDOW);
                expense = new Expense { ExpenseID = model.ExpenseId, Amount = model.ExpenseAmount, Description = model.Description, ExpenseDate = model.ExpenseDate, WeekNo = WeekNo };
                db.Entry(expense).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(expense);
        }

        // GET: Expenses/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expense expense = db.Expenses.Find(id);
            if (expense == null)
            {
                return HttpNotFound();
            }
            return View(expense);
        }

        // POST: Expenses/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Expense expense = db.Expenses.Find(id);
            db.Expenses.Remove(expense);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET : Get Weekly Report
        [Authorize]
        public ActionResult GetWeeklyReport([Bind(Include = "Start,End")] ExpenseWeeklyViewModel model)
        {
            // Gets the Calendar instance associated with a CultureInfo.
            CultureInfo myCI = new CultureInfo("en-US");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
            var id = User.Identity.GetUserId();
            var Bills = db.Expenses.Where(x => x.User.Id == id).ToList();
            if (model.Start != null && model.End != null)
            {
                Bills = Bills.Where(x => x.ExpenseDate >= model.Start && x.ExpenseDate <= model.End).ToList();
            }

            //var groupedByWeek = Bills.GroupBy(i => SqlFunctions.DatePart("week", i.ExpenseDate));
            ExpenseWeeklyViewModel expenseWeeklyViewModel = new ExpenseWeeklyViewModel();
            List<ExpenseWeeklyViewRecord> WeeklyReport = Bills.GroupBy(l => l.WeekNo).Select(cl => new ExpenseWeeklyViewRecord
            {
                //Start = FirstDateOfWeekISO8601(cl.First().ExpenseDate.Year, cl.First().WeekNo),
                //End = FirstDateOfWeekISO8601(cl.First().ExpenseDate.Year, cl.First().WeekNo).AddDays(6),
                Start = GetFirstDayOfWeek(cl.First().ExpenseDate.Year, cl.First().WeekNo, myCI).AddDays(-7),
                End = GetFirstDayOfWeek(cl.First().ExpenseDate.Year, cl.First().WeekNo, myCI).AddDays(-1),
                Sum = cl.Sum(c => c.Amount),
            }).OrderBy(x=>x.Start).ToList();
            expenseWeeklyViewModel.ExpenseRecords = WeeklyReport;
            return View(expenseWeeklyViewModel);
        }
        //GET : Get Admin Listing
        [Authorize(Roles ="Admin")]
        public ActionResult GetAdminList()
        {
            var Expenses = db.Expenses.ToList();
            List<ExpenseAdminListingViewModels> List1 = Expenses.Select(cl => new ExpenseAdminListingViewModels { FirstName = cl.User.FirstName, LastName = cl.User.LastName, Description = cl.Description, ExpenseAmount = cl.Amount, ExpenseDate = cl.ExpenseDate }).ToList();
            return View(List1);
        }
        // GET : Get Range
        [Authorize]
        public ActionResult GetRange()
        {
            return View();
        }

        // PUT: Get Range
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetRange([Bind(Include = "Start,End")] ExpenseViewRangeModels model)
        {
            var id = User.Identity.GetUserId();
            var Bills = db.Expenses.Where(x => x.User.Id == id).ToList();
            Bills = Bills.Where(x => x.ExpenseDate >= model.Start && x.ExpenseDate <= model.End).ToList();
            List<ExpenseWeeklyViewRecord> WeeklyReport = Bills.GroupBy(l => l.WeekNo).Select(cl => new ExpenseWeeklyViewRecord
            {
                Start = FirstDateOfWeekISO8601(cl.First().ExpenseDate.Year, cl.First().WeekNo),
                End = FirstDateOfWeekISO8601(cl.First().ExpenseDate.Year, cl.First().WeekNo).AddDays(6),
                Sum = cl.Sum(c => c.Amount),
            }).ToList();
            ExpenseWeeklyViewModel expenseWeeklyViewModel = new ExpenseWeeklyViewModel();
            expenseWeeklyViewModel.ExpenseRecords = WeeklyReport;
            return View("GetWeeklyReport", expenseWeeklyViewModel);

        }

        //Day from weekNo

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        public DateTime GetFirstDayOfWeek(int year, int weekNumber,
       System.Globalization.CultureInfo culture)
        {
            System.Globalization.Calendar calendar = culture.Calendar;
            DateTime firstOfYear = new DateTime(year, 1, 1, calendar);
            DateTime targetDay = calendar.AddWeeks(firstOfYear, weekNumber);
            DayOfWeek firstDayOfWeek = culture.DateTimeFormat.FirstDayOfWeek;

            while (targetDay.DayOfWeek != firstDayOfWeek)
            {
                targetDay = targetDay.AddDays(-1);
            }

            return targetDay;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
