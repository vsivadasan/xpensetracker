﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace XpenseTracker.Models
{

    //View Model for Expense
    public class ExpenseViewModels
    {
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Expense Date")]
        [DataType(DataType.DateTime)]
        public DateTime ExpenseDate { get; set; }
        [Required]
        [Display(Name = "Amount")]
        [DataType(DataType.Currency)]
        public double ExpenseAmount { get; set; }
        public int ExpenseId { get; set; }
    }
    public class ExpenseViewRangeModels
    {
        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Date")]
        public DateTime Start { get; set; }
        [Required]
        [Display(Name = "End Date")]
        [DataType(DataType.DateTime)]
        public DateTime End { get; set; }
    }
    public class ExpenseWeeklyViewRecord
    {
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DataType(DataType.DateTime)]
        public DateTime Start { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DataType(DataType.DateTime)]
        public DateTime End { get; set; }
        [DataType(DataType.Currency)]
        public double Sum { get; set; }
    }

    public class ExpenseWeeklyViewModel
    {
        public List<ExpenseWeeklyViewRecord> ExpenseRecords { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Date")]
        public DateTime Start { get; set; }
        [Required]
        [Display(Name = "End Date")]
        [DataType(DataType.DateTime)]
        public DateTime End
        { get; set; } = DateTime.Now;
    }

    public class ExpenseAdminListingViewModels
    {
        [Display(Name = "First Name")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Expense Date")]
        [DataType(DataType.DateTime)]
        public DateTime ExpenseDate { get; set; }
        [Required]
        [Display(Name = "Amount")]
        [DataType(DataType.Currency)]
        public double ExpenseAmount { get; set; }
        public int ExpenseId { get; set; }
    }
}