﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XpenseTracker.Models
{
    public class Expense
    {
        //Models Class for expense
        public int ExpenseID { get; set; }
        public int WeekNo { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public DateTime ExpenseDate { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}