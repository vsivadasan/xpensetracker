﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Web.Mvc;
using XpenseTracker.Controllers;
using XpenseTracker.Models;

namespace XpenseTracker.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {  
        [TestMethod]
        public void Login()
        {
            // Arrange
            AccountController controller = new AccountController();
            //Act
            ActionResult result = controller.Login("Test");
            //Assert
            Assert.IsNotNull(result);
        }

        // Login post
        [TestMethod]
        public void LoginPost()
        {
            // Arrange
            AccountController controller = new AccountController();
            //Act
            var temp = new LoginViewModel();
            temp.Email = "admin@administrator.com";
            temp.Password = "Admin@123";
            temp.RememberMe = false;
            Task<ActionResult> result = controller.Login(temp,  "Test");
            //Assert
            Assert.IsNotNull(result);
        }

        // Register
        [TestMethod]
        public void Register()
        {
            // Arrange
            AccountController controller = new AccountController();
            //Act
            Task<ActionResult> result = controller.Register(new RegisterViewModel());
            //Assert
            Assert.IsNotNull(result);
        }
    }

}
