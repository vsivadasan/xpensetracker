﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XpenseTracker.Controllers;
using System.Web.Mvc;
using XpenseTracker.Models;
using System.Security.Principal;
using System.Web;
using Moq;

namespace XpenseTracker.Tests.Controllers
{
    [TestClass]
    public class ExpenseControllerTest
    {
       
        
        [TestMethod]
        public void Details()
        {
            // Arrange
            ExpensesController controller = new ExpensesController();
            //Act
            ActionResult result = controller.Details(null);
            //Assert
            Assert.IsNotNull(result);
        }
       
  
       

        void Application_Start()
        {   
            ModelBinders.Binders[typeof(IPrincipal)] = new IPrincipalModelBinder();

        }
    }

    public class IPrincipalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }
            if (bindingContext == null)
            {
                throw new ArgumentNullException("bindingContext");
            }
            IPrincipal p = controllerContext.HttpContext.User;
            return p;
        }
    }


}
